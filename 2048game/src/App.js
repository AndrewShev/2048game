import React, { useState, useEffect}  from 'react';

function App() {
  //Уже времени мало, много ушло на создание проекта, задумка: есть таблица с элементами
  //при сдвиге будет выполняться такой ряд проверок, как в checkEvent
  //но вместо setitem01(null), будет выполняться что-то вроде подбрысывания монетки
  //(если рандом больше 0.5 => ставит 2, если нет null)
  //понимаю, что есть решения рациональней, но взял то, что было в голове на поверхности
  //+ постоянный просчет максимального значения, которого тут нет пока
  /////////////////
  const [item00, setitem00] = useState(null)
  const [item01, setitem01] = useState(2)
  const [item02, setitem02] = useState(null)
  //////////////////
  const [item10, setitem10] = useState(2)
  const [item11, setitem11] = useState(null)
  const [item12, setitem12] = useState(null)
  /////////////////
  const [item20, setitem20] = useState(null)
  const [item21, setitem21] = useState(2)
  const [item22, setitem22] = useState(null)

  const checkEvent = (event) => {
    switch (event.keyCode) {
      case 37:
        if (item00 === item01 === item02) {
          setitem00(item00 + item10 + item20)
          setitem01(null)
          setitem02(null)
        }
        if (item01 === item02 && item01 !== item00) {
          setitem01(item10 + item20)
          setitem02(null)
        }
          /////
        if (item10 === item11 === item12) {
          setitem00(item10 + item11 + item12)
          setitem01(null)
          setitem02(null)
        }
        if (item11 === item12 && item11 !== item10) {
          setitem01(item10 + item20)
          setitem02(null)
        }

        if (item00 === item01 === item02) {
          setitem00(item00 + item10 + item20)
          setitem01(null)
          setitem02(null)
        }
        if (item01 === item02 && item01 !== item00) {
          setitem01(item10 + item20)
          setitem02(null)
        }
        return
      case 38:
        if (item20 === item10 === item20) {
          setitem00(item00 + item10 + item20)
          setitem01(null)
          setitem02(null)
        }
        if (item10 === item20 && item10 !== item00) {
          setitem01(item10 + item20)
          setitem02(null)
        }
        return 
      case 39:
        return
      case 40:
        return
      default:
        return
    }
  }

  useEffect(() => checkEvent, [event])

  return (
    <div>
      <table>
        <thead>
          Wellcome to 2048 game
        </thead>
        <tbody>
          <tr>
            <td>
              {item00}
            </td>
            <td>
              {item01}
            </td>
            <td>
              {item02}
            </td>
          </tr>
          <tr>
            <td>
              {item10}
            </td>
            <td>
              {item11}
            </td>
            <td>
              {item12}
            </td>
          </tr>
          <tr>
            <td>
              {item20}
            </td>
            <td>
              {item21}
            </td>
            <td>
              {item22}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
